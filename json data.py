##### APPLICATION FOR MEDI-7 CLINICS
##### Author: VARUN MATHUR
##### Version 1.1
##### Date: 05 September 2018

##IMPORTING THE REQUIRED LIBRARIES

#Json library is imported as the data is in the json format and this library will be used to parse the josn data
#tkinter library is used to create the UI
#urllib is used to read the link(API) and load the data
#Datetime library is used to read the date that is inputted by the user
import json
from tkinter import *
from tkinter import messagebox
from tkinter.ttk import *
import urllib.request
import datetime

#Setting up the GUI
window = Tk()
window.title("Medi 7")
window.geometry('700x400')

###PLACEMENT OF THE VARIOS LABELS AND THE COMBOBOXES

#placing the label for the clnic name
lbl = Label(window, text="Clinic name")
lbl.grid(column=0, row=0)

##setting up the label for the date
lb4 = Label(window, text="Enter the date(DD/MM/YYYY)")
lb4.grid(column=10, row=0)

#Placing the textbox for getting the date as inputted by the user.
txt = Entry(window,width=20)
txt.grid(column=10, row=2)

#For doctor name
lb2 = Label(window, text="Dr name")
lb2.grid(column=0, row=10)

# Creating combobox for the doctor names
combo1 = Combobox(window)
combo1.grid(column=0, row=15)

combo2 = Combobox(window)

#The 6 clinic names will be displayed in the combobox. These are initialzed as follows.
combo2['values'] = ("Bentleigh", "Chadstone", "Clayton", "Lilydale", "Mooroolbark", "St Kilda")
combo2.grid(column=0, row=2)

##defining label for showing patient details
lb3 = Label(window, text="Patient details")
lb3.grid(column=10, row=16)

#Defining a list box to show all the patient details.
listbox = Listbox(window,width = 50)
listbox.grid(column=10, row=50)

#Setting up a dictionary for all the clinic names and their short names as the short names will be picked while
#creating the URL(API)
dict={"Bentleigh":"bent","Chadstone":"chad","Clayton":"clay","Lilydale":"lily","Mooroolbark":"moor","St Kilda":"stkd"}

##DEFINING THE FUNCTIONING OF THE BUTTONS
def clicked1():
    #clearing the items of the listbox and the combobox
    listbox.delete(0, END)
    combo1.items=[]
    #getting the value of the clinic name from the combobox
    value = combo2.get()
    #Getting the date from the textbox
    date = txt.get()
    #If the textbox is empty then the current date is taken, otherwise the entered date is conversted to a correct format
    #to retrive from the URL
    if len(date) == 0:
        new_date = datetime.datetime.today().strftime('%d-%m-%Y')
    else:
        new_date = datetime.datetime.strptime(date, '%d/%m/%Y').strftime('%d-%m-%Y')

    #Building up the URL
    base_url1 = "http://192.168.10.103:3333/api/appointment?codeAction=appointmentsByDate&codeSite="
    base_url2 = "&date="
    new_url = base_url1 + dict.get(value) + base_url2 + new_date
    print(new_url)
    #using the urllib library to open the newly created URL
    contents = urllib.request.urlopen(new_url).read()
    #Since the data is loadedin a binary format we decode the data and store the data in a new variable called "new"
    new=contents.decode("utf-8")
    #Converting this data into the json format and load the data and store it in a variable
    dr_dict = json.loads(new)

    # Getting the name of doctor and other appointment details.
    names = []
    new_dr = []
    dr_details = []
    for item in dr_dict:
        temp1 = item['nameDoctor']
        new_dr.append(temp1)
        for patient in item["appointments"]:
            temp2 = patient["namePatient"]
            temp3 = patient["appointmentTime"]
            new_dr.append(temp2)
            new_dr.append(temp3)
        dr_details.append(new_dr)
        new_dr = []
        names.append(temp1)
    for item in dr_details:
        names.append(item[0])
    new_names = set(names)
    name_tuple = tuple(new_names)
    if combo2.get() in dict.keys():
        combo1['values'] = name_tuple
    else:
        combo1['values'] = ''

    #Getting the patient name and the appointtime on click of the "get patients" button
    def clicked2():
        #Clearing the contents of the listbox initially
        listbox.delete(0, END)
        pt_name = []
        appt_time = []
        #Seperating the doctor name andthe appointment time and storing them into diffetent lists.
        for item in dr_details:
            if combo1.get() in item:
                new_list = item[1:]
                for value in new_list:
                    if value.isdigit() == FALSE:
                        pt_name.append(value)
                    else:
                        appt_time.append(value)

        #Converting the appointment times(milliseconds) to 24 hours format
        new_appt_time=[]
        for item in appt_time:
            millis=int(item)
            hours = (millis / 3600)
            new_24_time = datetime.timedelta(hours=hours)
            new_appt_time.append(str(new_24_time))

        #Populating the listbox
        for item in range(0, len(pt_name)):
            listbox.insert(END, pt_name[item] + " " * 10 + "-----" + " " * 7 + new_appt_time[item])
    #Defining the placement of the 'Get Patients' button
    btn2 = Button(window, text="Get Patients", command=lambda: clicked2())
    btn2.grid(column=2, row=14)

#Definig the placment of the 'Get doctors' button
btn1 = Button(window, text="Get Doctors", command=clicked1)
btn1.grid(column=1, row=0)

#Closing the application.
window.mainloop()